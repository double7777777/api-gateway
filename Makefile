CURRENT_DIR=$(shell pwd)
APP=template
APP_CMD_DIR=./cmd
run:
	go run cmd/main.go

build:
	CGO_ENABLED=0 GOOS=darwin go build -mod=vendor -a -installsuffix cgo -o ${CURRENT_DIR}/bin/${APP} ${APP_CMD_DIR}/main.go

proto-gen:
	./script/gen-proto.sh	${CURRENT_DIR}

lint: ## Run golangci-lint with printing to stdout
	golangci-lint -c .golangci.yaml run --build-tags "musl" ./...

swag:
	swag init -g ./api/router.go -o api/docs
