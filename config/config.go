package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment string // develop, staging, production

	UserServiceHost string
	UserServicePort string

	PostServiceHost string
	PostServicePort string

	CommentServiceHost string
	CommentServicePort string

	RedisHost string
	RedisPort string

	PostgresHost     string
	PostgresPort     string
	PostgresUser     string
	PostgresPassword string
	PostgresDatabase string

	SigninKey string

	CasbinConfigPath string
	CasbinPolicyPath string

	App               string
	OTLPCollectorHost string
	OTLPCollectorPort string

	// context timeout in seconds
	CtxTimeout int

	LogLevel string
	HTTPPort string
}

// Load loads environment vars and inflates Config
func Load() Config {
	c := Config{}

	c.UserServiceHost = cast.ToString(getOrReturnDefault("USER_SERVICE_HOST", "user_service"))
	c.UserServicePort = cast.ToString(getOrReturnDefault("USER_SERVICE_PORT", ":8000"))

	c.PostServiceHost = cast.ToString(getOrReturnDefault("POST_SERVICE_HOST", "post_service"))
	c.PostServicePort = cast.ToString(getOrReturnDefault("POST_SERVICE_PORT", ":8001"))

	c.CommentServiceHost = cast.ToString(getOrReturnDefault("COMMENT_SERVICE_HOST", "comment_service"))
	c.CommentServicePort = cast.ToString(getOrReturnDefault("COMMENT_SERVICE_PORT", ":8002"))

	c.RedisHost = cast.ToString(getOrReturnDefault("REDIS_HOST", "redis"))
	c.RedisPort = cast.ToString(getOrReturnDefault("REDIS_PORT", "6379"))

	c.App = cast.ToString(getOrReturnDefault("APP", "api"))
	c.OTLPCollectorHost = cast.ToString(getOrReturnDefault("OTLP_COLLECTOR_HOST", "localhost"))
	c.OTLPCollectorPort = cast.ToString(getOrReturnDefault("OTLP_COLLECTOR_PORT", ":4317"))

	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "godev"))
	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "database"))
	c.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT", "5434"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "yunus"))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "user_db"))

	c.SigninKey = cast.ToString(getOrReturnDefault("SIGNIN_KEY", "$2a$10$eFmgiRBf.ZdbDdeFIEbcL.lNP41vyArTisIM4kk.zVSF6VnR1SKUa"))

	c.CasbinConfigPath = cast.ToString(getOrReturnDefault("CASBIN_CONFIG_PATH", "./config/rbac_model.conf"))
	c.CasbinPolicyPath = cast.ToString(getOrReturnDefault("CASBIN_POLICY_PATH", "./config/rbac_policy.csv"))

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8081"))
	c.CtxTimeout = cast.ToInt(getOrReturnDefault("CTX_TIMEOUT", 7))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
