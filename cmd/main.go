package main

import (
	"fmt"

	"github.com/casbin/casbin/v2"
	defaultrolemanager "github.com/casbin/casbin/v2/rbac/default-role-manager"
	"github.com/casbin/casbin/v2/util"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"

	// gormadapter "github.com/casbin/gorm-adapter/v2"
	"github.com/double/api-gateway/api"
	"github.com/double/api-gateway/config"
	"github.com/double/api-gateway/pkg/logger"
	"github.com/double/api-gateway/pkg/otlp"
	"github.com/double/api-gateway/services"
	r "github.com/double/api-gateway/storage/redis"
	"github.com/gomodule/redigo/redis"
)

func main() {
	conf := jaegercfg.Configuration{
		Sampler: &jaegercfg.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 10,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans:           true,
			LocalAgentHostPort: "jaeger:6831",
		},
	}
	closer, err := conf.InitGlobalTracer(
		"api-gateway",
	)
	if err != nil {
		fmt.Println(err)
	}
	defer closer.Close()

	var (
		casbinEnforcer *casbin.Enforcer
	)

	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "api-gateway")

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.String("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	// psqlString := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
	// 	cfg.PostgresHost,
	// 	cfg.PostgresPort,
	// 	cfg.PostgresUser,
	// 	cfg.PostgresPassword,
	// 	cfg.PostgresDatabase)

	// a, err := gormadapter.NewAdapter("postgres", psqlString, true)
	// if err != nil {
	// 	log.Error("new adapter error", logger.Error(err))
	// 	return
	// }

	casbinEnforcer, err = casbin.NewEnforcer(cfg.CasbinConfigPath, cfg.CasbinPolicyPath)
	if err != nil {
		log.Error("new enforcer error", logger.Error(err))
		return
	}

	err = casbinEnforcer.LoadPolicy()
	if err != nil {
		log.Error("casbin load policy error", logger.Error(err))
		return
	}

	serviceManager, err := services.NewServiceManager(&cfg)
	if err != nil {
		log.Error("gRPC dial error", logger.Error(err))
	}

	shutdownOTLP, err := otlp.InitOTLPProvider(&cfg)
	if err != nil {
		log.Error("Init OTPL")
	}
	defer shutdownOTLP()

	pool := redis.Pool{
		MaxIdle:   80,
		MaxActive: 12000,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", fmt.Sprintf("%s:%s", cfg.RedisHost, cfg.RedisPort))
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}

	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch", util.KeyMatch)
	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch3", util.KeyMatch3)

	server := api.New(api.Config{
		Conf:            cfg,
		Logger:          log,
		ServiceManager:  serviceManager,
		ShutdownOTLP:    shutdownOTLP,
		InMemoryStorage: r.NewRedisRepo(&pool),
		CasbinEnforcer:  casbinEnforcer,
	})

	if err := server.Run(cfg.HTTPPort); err != nil {
		log.Fatal("failed to run http server", logger.Error(err))
		panic(err)
	}
}
