package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
	"github.com/double/api-gateway/pkg/otlp"
	"github.com/double/api-gateway/api/handlers/models"
	jwthandler "github.com/double/api-gateway/api/tokens"
	"github.com/double/api-gateway/email"
	pb "github.com/double/api-gateway/genproto/user"
	"github.com/double/api-gateway/pkg/etc"
	l "github.com/double/api-gateway/pkg/logger"
	"github.com/gin-gonic/gin"
	r "github.com/gomodule/redigo/redis"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Router  /v1/user/register	[post]
// @Summary Register
// @Description Section where users register
// @Tags Registration
// @Accept json
// @Produce json
// @Param body  body models.UserRegister true "UserRegistration"
// @Success	200 {object} models.StandartErrorModel
// @Failure 400 string  models.StandartErrorModel
func (h *handlerV1) Register(c *gin.Context) {
	var (
		body models.RegisterModel
	)
	
	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind JSON", l.Error(err))
		return
	}
	body.Email = strings.TrimSpace(body.Email)
	body.Email = strings.ToLower(body.Email)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "Register")
	defer span.End()


	existsEmail, err := h.serviceManager.UserService().CheckField(ctx, &pb.CheckRequest{
		Field: "email",
		Value: body.Email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Failed check email uniques", l.Error(err))
		return
	}

	if existsEmail.Exists {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
			"info":  "please enter another email",
		})
		h.log.Error("This email already exists", l.Error(err))
		return
	}

	code := etc.GenerateCode(6)

	msg := "Subject: Exam email verification\n Your verification code: " + code
	err = email.SendEmail([]string{body.Email}, []byte(msg))

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Errors{
			Error:       nil,
			Code:        http.StatusAccepted,
			Description: "Your Email is not valid, Please recheck it",
		})
		return
	}

	body.Code = code
	userBodyByte, err := json.Marshal(body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error while marshal user body", l.Error(err))
		return
	}

	err = h.redis.SetWithTTL(body.Email, string(userBodyByte), 300)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error set to redis user body", l.Error(err))
		return
	}
	c.JSON(http.StatusAccepted, code)
}

// @Router        /v1/user/verify/{email}/{code}	[post]
// @Summary       Verify
// @Description   Checks the user data from the database
// @Tags          Registration
// @Accept 		  json
// @Produce       json
// @Param  email  path string true "email"
// @Param  code   path string true "code"
// @Success	200   {object} models.VerifyResponse
// @Failure 400   string  models.StandartErrorModel
func (h *handlerV1) Verify(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	var (
		code  = c.Param("code")
		email = c.Param("email")
		body  models.RegisterModel

	)
	userBody, err := r.String(h.redis.Get(email))
	fmt.Println(" userbody>>>>>>>>>>", userBody)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error get from redis user body", l.Error(err))
		return
	}

	err = json.Unmarshal([]byte(userBody), &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error while unmarshaling user data", l.Error(err))
		return
	}

	if body.Code != code {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error while checking code", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "Verify")
	defer span.End()


	h.jwtHandler.Iss = "user"
	h.jwtHandler.Sub = body.Id
	h.jwtHandler.Role = "authorized"
	h.jwtHandler.Aud = []string{"exam-api"}
	h.jwtHandler.SigninKey= h.cfg.SigninKey
	h.jwtHandler.Log =h.log
	tokens, err := h.jwtHandler.GenerateAuthJWT()
	accessToken := tokens[0]
	refreshToken := tokens[1]
	
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error get from redis user body", l.Error(err))
		return
	}

	hashedPassword, err := etc.GeneratePasswordHash(body.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("verify:no hashed password", l.Error(err))
		return
	}

	body.AccessToken = accessToken
	body.RefreshToken = refreshToken

	user, err := h.serviceManager.UserService().CreateUser(ctx, &pb.UserRequest{
		Id:           body.Id,
		FirstName:    body.FirstName,
		LastName:     body.LastName,
		Email:        body.Email,
		Password:     string(hashedPassword),
		AccessToken:  body.AccessToken,
		RefreshToken: body.RefreshToken,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error while creating user to db", l.Error(err))
		return
	} 

	response := &models.VerifyResponse{
		Id:           user.Id,
		FirstName:    user.FirstName,
		LastName:     user.LastName,
		Email:        user.Email,
		Password:     string(hashedPassword),
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	c.JSON(http.StatusOK, response)
}

// @Router        /v1/user/login  [post]
// @Summary       Login
// @Description   User logs in based on their password
// @Tags          Registration
// @Accept 		  json
// @Produce       json
// @Param body  body models.LoginRequest true "UserLogin"
// @Success	200 {object} models.LoginResponse
// @Failure 400   string  models.StandartErrorModel
func (h *handlerV1) Login(c *gin.Context) {
	var (
		loginReq                  models.LoginRequest
		loginRes                  models.LoginResponse

	)

	err := c.ShouldBindJSON(&loginReq)
	if err != nil {
		return
	}
	res, err := h.serviceManager.UserService().Login(
		context.Background(), &pb.LoginRequest{
			Email:    loginReq.Email,
			Password: loginReq.Password,
		},
	)

	// message := "error while getting client by email"

	st, _ := status.FromError(err)
	if st != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("", l.Error(err))
		return
	}

	h.jwtHandler = jwthandler.JWTHandler{
		SigninKey: h.cfg.SigninKey,
		Sub:       res.Id,
		Iss:       "user",
		Role:      res.UserType,
		Log:       h.log,
	}
	fmt.Println(res.UserType)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "Login")
	defer span.End()

	tokens, err := h.jwtHandler.GenerateAuthJWT()
	accessToken := tokens[0]
	refreshToken := tokens[1]
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error get from redis user body", l.Error(err))
		return
	}

	ucReq := &pb.TokensUpdateReq{
		Id:           res.Id,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	_, err = h.serviceManager.UserService().UpdateUserTokens(
		context.Background(), ucReq,
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error get from redis user body", l.Error(err))
		return
	}

	loginRes.Id = res.Id
	loginRes.FirstName = res.FirstName
	loginRes.LastName = res.LastName
	loginRes.Email = loginReq.Email
	loginRes.UserType = res.UserType  //...........
	loginRes.AccessToken = accessToken
	loginRes.RefreshToken = refreshToken

	c.JSON(http.StatusOK, loginRes)
}
