package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	_ "github.com/double/api-gateway/api/handlers/models"
	pc "github.com/double/api-gateway/genproto/comment"
	l "github.com/double/api-gateway/pkg/logger"
	"github.com/double/api-gateway/pkg/otlp"
	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Router  /v1/comment/create [post]
// @Summary WriteComment
// @Description Write comment for post
// @Tags Comment service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body  body models.CommentRequest true "NewCommentRegister"
// @Success	200 {object} models.CommentResponse
// @Failure 400 string  Error response
func (h *handlerV1) WriteComment(c *gin.Context){
	var (
		body pc.CommentRequest
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind JSON", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "WriteComment")
	defer span.End()

	response, err := h.serviceManager.CommentService().WriteComment(ctx, &body)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create post", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// @Router  /v1/comment/get/{id} [get]
// @Summary PostComments
// @Description Outputs the post comments on the entered id
// @Tags Comment service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body  body models.IdRequest true "Wiew post comments"
// @Success	200 {object} models.PostResponse
// @Failure 400 string  Error response
func (h *handlerV1) PostComments(c *gin.Context){
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	id := c.Param("id")
	intID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to convert id to int", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "PostComments")
	defer span.End()

	response, err := h.serviceManager.CommentService().GetPostComments(ctx, &pc.IdRequest{PostId: int64(intID)})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error" : err.Error(),
		})
		h.log.Error("failed to get comment post by id", l.Error(err))
	}

	c.JSON(http.StatusOK, response)
}

// @Router  /v1/comment/all [get]
// @Summary AllComments
// @Description all comments can be seen
// @Tags Comment service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body  body models.Limit true "All posts by limit"
// @Success	200 {object} models.Comments
// @Failure 400 string  Error response
func (h *handlerV1) AllComments(c *gin.Context){
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	id := c.Param("id")
	intID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to convert id to int", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "AllComments")
	defer span.End()
	response, err := h.serviceManager.CommentService().GetAllComments(ctx, &pc.Limit{Limit: int64(intID)})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error" : err.Error(),
		})
		h.log.Error("failed to get all comments", l.Error(err))
	}

	c.JSON(http.StatusOK, response)
}

// @Router  /v1/comment/delete/{id} [delete]
// @Summary DeleteComment
// @Description Deletes the comment written for the post
// @Tags Comment service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body  body models.IdRequest true "Delete data"
// @Success	200 {object} models.PostResponse
// @Failure 400 string  Error response
func (h *handlerV1) DeleteComment(c *gin.Context){
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	newID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to convert id to int: ", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "DeleteComment")
	defer span.End()

	response, err:= h.serviceManager.CommentService().DeleteComment(ctx, &pc.IdRequest{PostId: int64(newID)})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error" : err.Error(),
		})
		h.log.Error("failed to delete comment", l.Error(err))
	}

	c.JSON(http.StatusOK, response)
}
