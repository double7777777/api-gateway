package v1

import (
	"errors"
	"net/http"

	"github.com/casbin/casbin/v2"
	"github.com/dgrijalva/jwt-go"
	"github.com/double/api-gateway/api/handlers/models"
	jwthandler "github.com/double/api-gateway/api/tokens"
	"github.com/double/api-gateway/config"
	"github.com/double/api-gateway/pkg/logger"
	"github.com/double/api-gateway/services"
	"github.com/double/api-gateway/storage/repo"
	"github.com/gin-gonic/gin"
	"go.opentelemetry.io/otel/trace"
)

type handlerV1 struct {
	log            logger.Logger
	serviceManager services.IServiceManager
	Tracer         trace.Tracer
	cfg            config.Config
	redis          repo.RedisRepo
	jwtHandler     jwthandler.JWTHandler
	CasbinEnforcer casbin.Enforcer
}

type HandlerV1Config struct {
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Tracer         trace.Tracer
	Cfg            config.Config
	Redis          repo.RedisRepo
	JwtHandler     jwthandler.JWTHandler
	Casbin         casbin.Enforcer
}

func New(l logger.Logger, s services.IServiceManager, c config.Config, r repo.RedisRepo, j jwthandler.JWTHandler, k casbin.Enforcer) *handlerV1 {
	return &handlerV1{
		log:            l,
		serviceManager: s,
		cfg:            c,
		redis:          r,
		jwtHandler:     j,
		CasbinEnforcer: k,
	}
}

func GetClaims(h *handlerV1, c *gin.Context) jwt.MapClaims {
	var (
		ErrUnauthorized = errors.New("unauthorized")
		authorization   models.GetProfileByJwtRequest
		claims          jwt.MapClaims
		err             error
	)

	authorization.Token = c.GetHeader("Authorization")
	if c.Request.Header.Get("Authorization") == "" {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  "ErrCodeUnauthorized",
				Message: "Unauthorized request",
			},
		})
		h.log.Error("Unauthorized request:", logger.Error(ErrUnauthorized))
		return nil
	}

	h.jwtHandler.Token = authorization.Token
	claims, err = h.jwtHandler.ExtractClaims()
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  "ErrCodeUnauthorized",
				Message: "Unauthorized request",
			},
		})
		h.log.Error("Unauthorized request", logger.Error(err))
		return nil
	}
	return claims
}
