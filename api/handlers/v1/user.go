package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	_ "github.com/double/api-gateway/api/handlers/models"
	pb "github.com/double/api-gateway/genproto/user"
	l "github.com/double/api-gateway/pkg/logger"
	"github.com/double/api-gateway/pkg/otlp"
	"github.com/double/api-gateway/pkg/utils"
	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Summary CreateUser
// @Description Through this api can user register
// @Tags User service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body  body models.UserReq true "CreateRegister"
// @Success	200 {object} models.UserRes
// @Failure 400 string  Error response
// @Router  /v1/users/create	[post]
func (h *handlerV1) CreateUser(c *gin.Context) {
	var (
		body        pb.UserRequest
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "CreateUser")
	defer span.End()

	response, err := h.serviceManager.UserService().CreateUser(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create user", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// @Router  /v1/users/get/{id}	[get]
// @Summary GetUser
// @Description Returns the user with the entered id
// @Tags User service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param body  body models.IdRequest true "Enter an ID"
// @Success	200 {object} models.UserRes
// @Failure 400 string  Error response
func (h *handlerV1) GetUserById(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	id := c.Param("id")
	intID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "Invalid Info",
		})
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetUserById")
	defer span.End()
	response, err := h.serviceManager.UserService().GetUserById(
		ctx, &pb.UserId{
			Id: int64(intID),
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Router  /v1/users/all	[get]
// @Summary GetAllUsers
// @Description Outputs users by page and limit
// @Tags User service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body  body models.ListRequest true "Wiew all users"
// @Success	200 {object} models.UsersList
// @Failure 400 string  Error response
func (h *handlerV1) GetAllUsers(c *gin.Context) {
	queryParams := c.Request.URL.Query()

	params, errStr := utils.ParseQueryParams(queryParams)
	if errStr != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": errStr[0],
		})
		h.log.Error("failed to parse query params json" + errStr[0])
		return
	}

	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetAllUsers")
	defer span.End()
	response, err := h.serviceManager.UserService().GetAllUsers(
		ctx, &pb.UserListReq{
			Limit: params.Limit,
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to list users", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Router  /v1/users/update/{id}	[put]
// @Summary UpdateUser
// @Description Updates user data in id
// @Tags User service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param body  body models.UpdateRequest true "Update data"
// @Success	200 {object} models.UserRes
// @Failure 400 string  Error response
func (h *handlerV1) UpdateUser(c *gin.Context) {
	var (
		body        pb.UserUpdateReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}

	newID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to convert id to int", l.Error(err))
	}
	body.Id = int64(newID)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "UpdateUser")
	defer span.End()
	response, err := h.serviceManager.UserService().UpdateUser(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update user", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Router  /v1/users/delete/{id}	[delete]
// @Summary DeleteUser
// @Description Deletes the entered id data
// @Tags User service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param body  body models.IdRequest true "Delete data"
// @Success	200 {object} models.UserRes
// @Failure 400 string  Error response
func (h *handlerV1) DeleteUser(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	newID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to convert id to int: ", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "DeleteUser")
	defer span.End()
	response, err := h.serviceManager.UserService().DeleteUser(
		ctx, &pb.UserDeleteReq{
			Id: int64(newID),
		})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to delete user", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Router  /v1/users/post/{id}	[get]
// @Summary GetUserByPostId
// @Description Takes the user of the post
// @Tags User service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param body  body models.PostId true "Wiew post creator"
// @Success	200 {object} models.UserResponseForPost
// @Failure 400 string  Error response
func (h *handlerV1) GetUserByPostId(c *gin.Context) {
	jspbMarshal := protojson.MarshalOptions{}
	jspbMarshal.UseProtoNames = true

	intID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to convert id to int: ", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetUserByPostId")
	defer span.End()

	response, err := h.serviceManager.UserService().GetUserByPostId(ctx, &pb.PostId{PostId: int64(intID)})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to user by posts", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Router  /v1/users/check	[get]
// @Summary CheckField
// @Description Checks that the user's email is unique
// @Tags User service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param body  body models.CheckRequest true "Checked users email"
// @Success	200 {object} models.CheckResponse
// @Failure 400 string  Error response
func (h *handlerV1) CheckField(c *gin.Context) {
	var (
		body        pb.UserUpdateReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "CheckField")
	defer span.End()

	response, err := h.serviceManager.UserService().CheckField(ctx, &pb.CheckRequest{
		Field: "email",
		Value: body.Email,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to checkfield", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}
