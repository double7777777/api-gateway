package v1

import (
	"fmt"
	"net/http"

	"github.com/double/api-gateway/api/handlers/models"
	"github.com/gin-gonic/gin"
)


// Superuser
// @Router /v1/rbac/add-policy [post]
// @Summary AddRoleUser
// @Tags RBAC
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param body body models.Policy true "Create new role"
// @Success 200 {object} models.SuccessInfo
// @Failture 400 string Error models.Error
// @Failture 500 string Error models.Error
func(h *handlerV1) AddRoleUser(c *gin.Context){
	body := models.Policy{}

	err := c.ShouldBindJSON(&body)
	if err != nil{
		fmt.Println(err)
	}

	ok, err := h.CasbinEnforcer.AddPolicy(body.User, body.Domain, body.Action)
	if err != nil{
		fmt.Println(">>>", err)
	}

	h.CasbinEnforcer.SavePolicy()
	fmt.Println(ok)
	c.JSON(http.StatusOK, models.SuccessInfo{
		Message: "Success added policy",
	})
}


// Superuser
// @Router /v1/rbac/removed-policy [post]
// @Summary RemovedUser
// @Tags RBAC
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param body body models.Policy true "Removed role"
// @Success 200 {object} models.SuccessInfo
// @Failture 400 string Error models.Error
// @Failture 500 string Error models.Error
func(h *handlerV1) RemovePolicy(c *gin.Context){
	body := models.Policy{}

	err := c.ShouldBindJSON(&body)
	if err != nil{
		fmt.Println(err)
	}

	ok, err := h.CasbinEnforcer.RemovePolicy(body.User, body.Domain, body.Action)
	if err != nil{
		fmt.Println(">>>", err)
	}

	h.CasbinEnforcer.SavePolicy()
	fmt.Println(ok)
	c.JSON(http.StatusOK, models.SuccessInfo{
		Message: "Success removed policy",
	})
}