package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	_ "github.com/double/api-gateway/api/handlers/models"
	pp "github.com/double/api-gateway/genproto/post"
	l "github.com/double/api-gateway/pkg/logger"
	"github.com/double/api-gateway/pkg/otlp"
	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// @Router /v1/post/create [post]
// @Summary CreatePost
// @Description Users create a post
// @Tags Post service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body  body models.PostRequest true "PostCreating"
// @Success	200 {object} models.PostResponse
// @Failure 400 string  Error response
func (h *handlerV1) CreatePost(c *gin.Context) {
	var (
		body pp.PostRequest
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind JSON", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "CreatePost")
	defer span.End()

	response, err := h.serviceManager.PostService().CreatePost(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create post", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)

}

// @Router  /v1/post/get/{id} [get]
// @Summary Get post
// @Description Returns the post with the entered id
// @Tags Post service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body body models.IdRequestP true "Enter an ID"
// @Success	200 {object} models.PostResponse
// @Failure 400 string  Error response
func (h *handlerV1) GetPost(c *gin.Context){
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	id := c.Param("id")
	intID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to convert id to int", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "GetPost")
	defer span.End()

	response, err := h.serviceManager.PostService().GetPostById(ctx, &pp.PostId{Id: int64(intID)})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get post by id", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)

}

// @Router  /v1/post/update/{id} [put]
// @Summary UpdatePost
// @Description Updates post data in id
// @Tags Post service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body  body models.UpdatePostRequest true "Update data"
// @Success	200 {object} models.PostResponse
// @Failure 400 string  Error response
func (h *handlerV1) UpdatePost(c *gin.Context) {
	var (
		body pp.UpdatePostRequest
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind JSON", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "UpdatePost")
	defer span.End()

	response, err := h.serviceManager.PostService().UpdatePost(ctx, &body)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update post", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// @Router  /v1/post/delete/{id} [delete]
// @Summary DeletePost
// @Description  Deletes the entered id data
// @Tags Post service
// @Security BearerAuth
// @Accept json
// @Produce json
// @Param 	body  body models.IdRequestP true "Delete data"
// @Success	200 {object} models.PostResponse 
// @Failure 400 string  Error response
func (h *handlerV1) DeletePost(c *gin.Context) {
	jspbMarshal := protojson.MarshalOptions{}
	jspbMarshal.UseProtoNames = true

	newID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to convert id to int: ", l.Error(err))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	ctx, span := otlp.Start(ctx, "handler", "DeletePost")
	defer span.End()
	response, err := h.serviceManager.PostService().UpdatePost(ctx, &pp.UpdatePostRequest{Id: int64(newID)})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to delete post", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}
