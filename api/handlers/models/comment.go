package models

type CommentRequest struct{
    PostId int `json:"post_id"`
    OwnerId int `json:"owner_id"`
    Description string `json:"description"`
}

type CommentResponse struct{
    Id int `json:"id"`
    PostId int `json:"post_id"`
    OwnerId int `json:"owner_id"`
    Fullname string `json:"fullname"`
    Description string `json:"description"`
    CreatedAt string `json:"created_at"`
}

type Comments struct{
    Comments []CommentResponse `json:"comments"`
}

type Limit struct{
    Limit int `json:"limit"`
}
