package models

type Errors struct {
	Code        int
	Error       error
	Description string
}

type SuccessInfo struct {
	Message    string `json:"message"`
	StatusCode int    `json:"status_code"`
}

// Error ...
type Error struct {
	Message string `json:"message"`
}

// StandartErrorModel ...
type StandartErrorModel struct {
	Error Error `json:"error"`
}