package models

type PostRequest struct{
    Title string `json:"title"`
    Description string `json:"description"`
    OwnerId int `json:"owner_id"`
}

type PostResponse struct{
    Id int `json:"id"`
    Fullname string `json:"fullname"`
    OwnerId int `json:"owner_id"`
    Title string `json:"title"`
    Description string `json:"description"`
    CreatedAt string `json:"created_at"`
    UpdatedAt string `json:"updated_at"`
}

type Posts struct{
    Posts []PostResponse `json:"posts"`
}

type UpdatePostRequest struct{
    Title string `json:"title"`
    Description string `json:"description"`
    Id int `json:"id"`
}

type IdRequestP struct {
	Id int `json:"id"`
}