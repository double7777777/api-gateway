package models

type UserReq struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

type UserRes struct {
	Id        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type IdRequest struct {
	Id int `json:"id"`
}

type ListRequest struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
}

type UsersList struct {
	Users []UserRes `json:"users"`
}

type UpdateRequest struct {
	Id        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

type RegisterModel struct {
	Id           string `json:"id"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Email        string `json:"email"`
	Password     string `json:"password"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	Code         string `json:"code"`
}

type PostId struct {
	Post_id int `json:"post_id"`
}

type UserResponseForPost struct {
	Id        int    `json:"id"`
	Post_id   int    `json:"post_id"`
	FirstName string `json:"first_name"`
	CreatedAt string `json:"created_at"`
}

type Users struct {
	Users []UserRes `json:"users"`
}

type UserUpdateRequest struct {
	Id        int64  `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

type CheckRequest struct {
	Field string `json:"field"`
	Value string `json:"value"`
}

type CheckResponse struct {
	Exists bool `json:"exists"`
}
