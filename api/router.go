package api

import (
	"github.com/casbin/casbin/v2"
	_ "github.com/double/api-gateway/api/docs" //swag
	v1 "github.com/double/api-gateway/api/handlers/v1"
	"github.com/double/api-gateway/api/middleware"
	jwthandler "github.com/double/api-gateway/api/tokens"
	"github.com/double/api-gateway/config"
	"github.com/double/api-gateway/pkg/logger"
	"github.com/double/api-gateway/services"
	"github.com/double/api-gateway/storage/repo"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerfile "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// Option ...
type Config struct {
	Conf            config.Config
	Logger          logger.Logger
	ShutdownOTLP    func() error
	ServiceManager  services.IServiceManager
	InMemoryStorage repo.RedisRepo
	CasbinEnforcer  *casbin.Enforcer
}

// New ...
// @title  Yunus Karimboyev
// @version 2.0
// @description Swagger bilan ishlash ko'nikmasini shakllantirish..!
// @contact.url  https://t.me/DistantSEA
// @contact.email karimboyevyunus@gmail.com
// @host	localhost:8081
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func New(option Config) *gin.Engine {
	router := gin.New()

	corConfig := cors.DefaultConfig()
	corConfig.AllowAllOrigins = true
	corConfig.AllowCredentials = true
	corConfig.AllowHeaders = []string{"*"}
	corConfig.AllowBrowserExtensions = true
	corConfig.AllowMethods = []string{"*"}

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	jwtHandler := jwthandler.JWTHandler{
		SigninKey: option.Conf.SigninKey,
		Log:       option.Logger,
	}
	router.Use(middleware.NewAuthorizer(option.CasbinEnforcer, jwtHandler, option.Conf))

	handlerV1 := v1.New(
		option.Logger,
		option.ServiceManager,
		option.Conf,
		option.InMemoryStorage,
		jwtHandler,
		*option.CasbinEnforcer,
	)

	api := router.Group("/v1")
	// users
	api.POST("/users/create", handlerV1.CreateUser)
	api.GET("/users/get/:id", handlerV1.GetUserById)
	api.GET("/users/all", handlerV1.GetAllUsers)
	api.PUT("/users/update/:id", handlerV1.UpdateUser)
	api.DELETE("/users/delete/:id", handlerV1.DeleteUser)
	api.GET("/users/post/:id", handlerV1.GetUserByPostId)
	api.GET("/users/check", handlerV1.CheckField)

	// // posts

	api.POST("/post/create", handlerV1.CreatePost)
	api.GET("/post/get/:id", handlerV1.GetPost)
	api.PUT("/post/update/:id", handlerV1.UpdatePost)
	api.DELETE("/post/delete/:id", handlerV1.DeletePost)

	// // comments

	api.POST("/comment/create", handlerV1.WriteComment)
	api.GET("/comment/get/:id", handlerV1.PostComments)
	api.GET("/comment/all", handlerV1.AllComments)
	api.DELETE("/comment/delete/:id", handlerV1.DeleteComment)

	// register

	api.POST("/user/register", handlerV1.Register)
	api.POST("/user/verify/:email/:code", handlerV1.Verify)
	api.POST("/user/login", handlerV1.Login)

	// RBAC

	api.POST("/rbac/add-policy", handlerV1.AddRoleUser)
	api.POST("/rbac/removed-policy", handlerV1.RemovePolicy)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfile.Handler, url))

	return router
}
