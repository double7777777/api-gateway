package middleware

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/casbin/casbin/v2"
	jwtg "github.com/dgrijalva/jwt-go"
	"github.com/double/api-gateway/api/handlers/models"
	jwthandler "github.com/double/api-gateway/api/tokens"
	"github.com/double/api-gateway/config"
	"github.com/gin-gonic/gin"
)

type JwtRoleAuth struct {
	enforcer *casbin.Enforcer
	cfg      config.Config
	jwtHandler jwthandler.JWTHandler
}

func NewAuthorizer(enforcer *casbin.Enforcer, jwtHandler jwthandler.JWTHandler, cfg config.Config) gin.HandlerFunc{
	a := &JwtRoleAuth{
		enforcer: enforcer,
		cfg: cfg,
		jwtHandler: jwtHandler,
	}

	return func(c *gin.Context) {
		allow, err := a.CheckPermission(c.Request)
		fmt.Println(allow)
		if err != nil {
			v, _ := err.(jwtg.ValidationError)
			if v.Errors == jwtg.ValidationErrorExpired {
				a.RequireRefresh(c)
			} else {
				a.RequirePermission(c)
			}
		} else if ! allow {
			a.RequirePermission(c)
		}
	}
}

func (a *JwtRoleAuth) GetRole(r *http.Request) (string, error){
	var (
		role string
		claims jwtg.MapClaims
		err error
	)
	jwtToken := r.Header.Get("Authorization")
	// fmt.Println("200:", jwtToken)
	if jwtToken == "" {
		return "unauthorized", nil
	} else if strings.Contains(jwtToken, "Basic"){
		return "unauthorized", nil
	}

	a.jwtHandler.Token = jwtToken

	claims, err = a.jwtHandler.ExtractClaims()

	if err != nil {
		fmt.Println(err)
		return "", err
	}

	if claims["role"].(string) == "user" {
		role = "user"

	} else if claims["role"].(string) == "admin" {
		role = "admin"

	}else if claims["role"].(string) == "superuser" {
		role = "superuser"

	} else if claims["role"].(string) == "moderator" {
		role = "moderator"

	} else {
		role = "unknown"
	}
	fmt.Println(role)
	return role, nil
}

func (a *JwtRoleAuth) CheckPermission(r *http.Request) (bool, error) {
	user, err := a.GetRole(r)
	if err != nil {
		fmt.Println(err)
		return false, err
	}
	method := r.Method
	path := r.URL.Path
	// fmt.Println("aaaaa",user, path, method)
	allowed, err := a.enforcer.Enforce(user, path, method)

	if err != nil{
		return false, err
	}
	return allowed, nil
}

func (a *JwtRoleAuth) RequirePermission(c *gin.Context){
	c.AbortWithStatus(403)
}

func (a *JwtRoleAuth) RequireRefresh(c *gin.Context){
	c.JSON(http.StatusUnauthorized, models.ResponseError{
		Error: models.ServerError{
			Status: "UNAUTHORIZED",
			Message: "Token is expired",
		},
	})
	c.AbortWithStatus(401)
}