package services

import (
	"fmt"

	"github.com/double/api-gateway/config"
	pc "github.com/double/api-gateway/genproto/comment"
	ps "github.com/double/api-gateway/genproto/post"
	pb "github.com/double/api-gateway/genproto/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"
)

type IServiceManager interface {
	UserService() pb.UserServiceClient
	PostService() ps.PostServiceClient
	CommentService() pc.CommentServiceClient
}

type serviceManager struct {
	userService    pb.UserServiceClient
	postService    ps.PostServiceClient
	commentService pc.CommentServiceClient
}

func (s *serviceManager) UserService() pb.UserServiceClient {
	return s.userService
}

func (s *serviceManager) PostService() ps.PostServiceClient {
	return s.postService
}

func (s *serviceManager) CommentService() pc.CommentServiceClient {
	return s.commentService
}

func NewServiceManager(conf *config.Config) (IServiceManager, error) {
	resolver.SetDefaultScheme("dns")

	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.UserServiceHost, conf.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.PostServiceHost, conf.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	connComment, err := grpc.Dial(
		fmt.Sprintf("%s:%s", conf.CommentServiceHost, conf.CommentServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	serviceManager := &serviceManager{
		userService:    pb.NewUserServiceClient(connUser),
		postService:    ps.NewPostServiceClient(connPost),
		commentService: pc.NewCommentServiceClient(connComment),
	}

	return serviceManager, nil
}
